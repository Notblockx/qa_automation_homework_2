package domashka2.tests;

import domashka2.DriverInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CheckMainMenuTest extends DriverInit {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = initDriver(); // ініціалізація драйвера
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement loginElement = driver.findElement(By.name("email"));
        loginElement.sendKeys("webinar.test@gmail.com");
        WebElement passElement = driver.findElement(By.name("passwd"));
        passElement.sendKeys("Xcg7299bnSmMuRLp9ITw");
        WebElement enter = driver.findElement(By.name("submitLogin"));
        enter.click();
        sleep(); // Thread.sleep() - 2 сек
        List<WebElement> linkss = driver.findElements(By.className("maintab"));
        for (int i = 0; i < linkss.size(); i++) {
            if ( i == 2 || i == 6 ) { continue; } // пропускаю пункти "Каталог" і "Modules"
            /* на сайті криво зверстані ці дві сторінки, елементи в список вибираю по класу .maintab
             і на пунктах "Каталог" і "Modules" метод findElement() знаходить 0 елементів.
             $('.maintab').length показує 0 елементів, а має бути 13 елементів з класом maintab */
            List<WebElement> list = driver.findElements(By.className("maintab"));
            list.get(i).click();
            System.out.print(driver.findElement(By.className("page-title")).getText() + " - " );
            driver.navigate().refresh();
            System.out.println(driver.findElement(By.className("page-title")).getText());
            sleep(); // Thread.sleep() - 2 сек
        }
        driver.quit();
    }
}

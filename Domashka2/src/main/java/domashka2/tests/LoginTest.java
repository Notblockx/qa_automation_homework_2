package domashka2.tests;

import domashka2.DriverInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginTest extends DriverInit {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = initDriver(); // ініціалізація драйвера
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement loginElement = driver.findElement(By.name("email"));
        loginElement.sendKeys("webinar.test@gmail.com");
        WebElement passElement = driver.findElement(By.name("passwd"));
        passElement.sendKeys("Xcg7299bnSmMuRLp9ITw");
        WebElement enter = driver.findElement(By.name("submitLogin"));
        enter.click();
        sleep(); // Thread.sleep() - 2 сек
        System.out.println("Page title is: " + driver.getTitle());
        WebElement employeeInfo = driver.findElement(By.id("header_employee_box"));
        employeeInfo.click();
        sleep(); // Thread.sleep() - 2 сек
        WebElement exit = driver.findElement(By.id("header_logout"));
        exit.click();
        sleep(); // Thread.sleep() - 2 сек
        System.out.println("Page title is: " + driver.getTitle());
        driver.quit();
    }
}

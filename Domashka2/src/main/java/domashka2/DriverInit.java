package domashka2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

    public class DriverInit {

       public static WebDriver initDriver() {
            WebDriverManager.chromedriver().setup();
            return new ChromeDriver();
        }

        public static void sleep(){
           try {
               Thread.sleep(2000);
           }
           catch (InterruptedException e) {
               System.out.println("Exception");
               e.printStackTrace();
           }
       }
    }


